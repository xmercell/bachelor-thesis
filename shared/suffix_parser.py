import re
from datetime import date

def get_publication_year_from_suffix(suffix):
    """This function retrieves year from bibliographic references

    Args:
        suffix (str): Bibliographic references which contains year 

    Returns:
        str: Year if was found by regular expression and satisfy some conditions, empty string otherwise
    """
    start_year = 1700
    end_year = date.today().year + 1
    match = re.search("[^-–0-9]?([0-9]{4})[^-–0-9]?", suffix)
    if match is None:
        return ""
    year = int(match.group(1))
    if year >= start_year and year <= end_year:
        return year
    return ""


def get_page_range_from_suffix(suffix):
    """This function retrieves page range from bibliographic references

    Args:
        suffix (str): Bibliographic references which contains year 

    Returns:
        Tuple[str,str]: first page and last page in page range if was found by regular expression and satisfy some conditions, empty string and empty string otherwise
    """
    match = re.search("\d{1,}[–-]\d{1,}", suffix)
    if match is None:
        return "", ""
    first_page, last_page = re.split("–|-", match[0])
    if first_page is not None and last_page is not None:
        return first_page, last_page
    return "", ""

def get_one_author(author):
    """When there are plenty of authors in author variable it identifies one surname of one author

    Args:
        author (str): One or more authors of a publication

    Returns:
        str: One author surname
    """
    regex = "[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð]{1,}"
    match = re.search(regex,author)
    if match is None:
        return ""
    return match[0]