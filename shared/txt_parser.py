def create_query_using_attributes_txt(meta_data, query):
    """This function takes meta_data consisting of author, title and bibliographic references and modifies given dictionary

    Args:
        meta_data (Dict[str]): Dictionary of author, title and bibliographic references
        query (Dict[str]): Modified dictionary of author, title and bibliographic references
    Returns:
        Optional[str]: string if given meta_data has not separated author, title, bibliographic references, None otherwise
    """
    data = meta_data.split("//")
    if len(data) < 3:
        return "NO DATA"
    authors, title, biblographic = data[0].split(" ")[1], data[1], data[2]
    query["query.author"] = authors
    query["query.bibliographic"] = biblographic
    query["query.title"] = title


def parse_txt(txt_data,parser):
    """If user is using Content-Type as text/plain this function processes the content of the request

    Args:
        txt_data (bytes): Bytes of text string
        parser (Callable): Function, which is going to retrieve the specified identifier

    Returns:
        Dict[str]: Dictionary which contains meta data from user extended by the specified identifier (DOI, ZBL, MR)
    """
    query = {}
    output = []
    books = str(txt_data,"utf-8").split('\n')
    for book in books:
        if create_query_using_attributes_txt(book, query) == "NO DATA":
            output.append({
                "author": "",
                "bibliographic": "",
                "doi": "",
                "doi score": "",
                "doi_link": "",
                "title": "",
            })
            query = {}
            continue
       
        identifier = parser(query)

        output.append({
            "author": query["query.author"],
            "title": query["query.title"],
            "bibliographic": query["query.bibliographic"],
        } | identifier)
        query = {}
    return output