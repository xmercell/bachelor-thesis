import xml.etree.ElementTree as ET


def create_query_using_attributes_xml(meta_data, query):
    """This function takes meta_data consisting of author, title and bibliographic references and modifies given dictionary

    Args:
        meta_data (Dict[str]): Dictionary of author, title and bibliographic references
        query (Dict[str]):  Modified dictionary of author, title and bibliographic references
    """
    if meta_data.tag == "authors":
        authors = []
        for author in meta_data:
            authors.append(author.text)
        query["query.author"] = ", ".join(list(authors))
    if meta_data.tag == "suffix":
        query["query.bibliographic"] = meta_data.text
    if meta_data.tag == "title":
        query["query.title"] = meta_data.text


def parse_xml(xml_data, parser):
    """If user is using Content-Type as application/xml this function processes the content of the request

    Args:
        xml_data (bytes): Bytes of xml
        parser (Callable): Function, which is going to retrieve the specified identifier

    Returns:
        Dict[str]: Dictionary which contains meta data from user extended by the specified identifier (DOI, ZBL, MR)
    """
    output = []
    query = {}
    references = ET.fromstring(str(xml_data, "utf-8"))
    for reference in references:
        for meta_data in reference:
            create_query_using_attributes_xml(meta_data, query)

        identifier = parser(query)

        output.append({
            "author": query["query.author"],
            "title": query["query.title"],
            "bibliographic": query["query.bibliographic"],
        } | identifier)
        query = {}
    return output
