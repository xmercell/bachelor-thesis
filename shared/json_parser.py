import json

def create_query_using_attributes_json(meta_data, query):
    """This function takes meta_data consisting of author, title and bibliographic references and modifies given dictionary

    Args:
        meta_data (Dict[str]): Dictionary of author, title and bibliographic references
        query (Dict[str]): Modified dictionary of author, title and bibliographic references
    """
    query["query.author"] = meta_data["author"]
    query["query.bibliographic"] = meta_data["bibliographic"]
    query["query.title"] = meta_data["title"]


def parse_json(json_data,parser):
    """If user is using Content-Type as application/xml this function processes the content of the request

    Args:
        json_data (bytes): Bytes of json
        parser (Callable): Function, which is going to retrieve the specified identifier

    Returns:
        Dict[str]: Dictionary which contains meta data from user extended by the specified identifier (DOI, ZBL, MR)
    """
    query = {}
    output = []
    data = json.loads(str(json_data,"utf-8"))
    books = [data] if isinstance(data,dict) else data
    for book in books:
        create_query_using_attributes_json(book, query)
        
        identifier = parser(query)

        output.append({
            "author": query["query.author"],
            "title": query["query.title"],
            "bibliographic": query["query.bibliographic"],
        } | identifier)
        query = {}

    return output