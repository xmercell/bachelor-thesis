from doi import doi_parser
from zbl import zbl_parser
from mr import mr_parser
from mr import mr_advanced_parser
from mr import mr_advanced_finder

def parser(query):
    """This function is used as a parser to retrieve all identifiers (DOI, MR, ZBL)

    Args:
        query (Dict[str]): Dictionary, which contains basic information such as author, title and bibliographic references

    Returns:
        Dict[str]: Dictionary which contains all identifiers (DOI, MR, ZBL)
    """
    doi_identifier = doi_parser.parser(query)
    zbl_identifier = zbl_parser.parser(query)
    mr_identifier = ""
    if mr_advanced_finder.check_access_to_advanced_search():
        mr_identifier = mr_advanced_parser.parser(query)
    else:
        mr_identifier = mr_parser.parser(query)
    return doi_identifier | zbl_identifier | mr_identifier
