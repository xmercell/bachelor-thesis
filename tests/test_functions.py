import doi 
import zbl
import mr

def create_query(author,title,bibliographic):
    query = {}
    query["query.author"] = author
    query["query.bibliographic"] = bibliographic
    query["query.title"] = title
    return query


def test_doi_parser():
    """This function is testing if DOI is retrieved correctly from Crossref API
    """
    expected_result = "10.1007/bf01352146"
    query = create_query("Dinghas A.","Superadditive Funktionale, Identitäten und Ungleichungen der elementaren Analysis","Math. Ann., vol. 178, 1968, 315-334.")
    actual_result = doi.parser(query)["doi"]
    assert expected_result == actual_result

def test_zbl_parser_one_result_on_page():
    """This function is testing if ZBL is retrieved correctly, if there is only one result on zbMATH website
    """
    expected_result = "1150.11339"
    query = create_query("Grekos, G","On various definitions of density","(survey), Tatra Mt. Math. Publ. 31 (2005) 17–27")
    actual_result = zbl.parser(query)["zbl"][4:]
    assert expected_result == actual_result

def test_zbl_parser_more_results_on_page():
    """This function is testing if ZBL is retrieved correctly, if there are a few results on zbMATH website
    """
    expected_result = "0880.11001"
    query = create_query("Tenenbaum, G.","Introduction to analytic and probabilistic number theory","Cambridge Univ. Press, Cambridge, 1995")
    actual_result = zbl.parser(query)["zbl"][4:]
    assert expected_result == actual_result

def test_mr_parser():
    """This function is testing if MR is retrieved correctly, if basic attributes are used from website MrLookup and MRef
    """
    expected_result = "MR2397737"
    query = create_query("Taylor, M. D.","Multivariate measures of concordance.","Ann. Inst. Statist. Math. 59 (2007), no. 4, 789-806")
    actual_result = mr.parser(query)["mr"]
    assert expected_result == actual_result

def test_mr_advanced_parser():
    """This function is testing if MR is retrieved correctly, if extended attributes are used from website MathSciNet, user must have subscription on this website
    """
    expected_result = "MR0620638" if mr.check_access_to_advanced_search() else ""
    query = create_query("Fučík S.","Solvability of nonlinear equations and boundary value problems","Reidel, Dordrecht, 1980.")
    actual_result = mr.advanced_parser(query)["mr"]
    assert expected_result == actual_result
