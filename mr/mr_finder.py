import requests
from ratelimit import limits, sleep_and_retry
import shared


@sleep_and_retry
@limits(calls=50,
        period=1)  # Allows 50 requets within 1 second; sleep till the request period has ellapsed and then retry
def find_mr_and_metadata(author, title, suffix):
    """This function makes an GET request to Batch MrLookup and retrieves MR with its link

    Args:
        author (str): Author of the publication
        title (str): Title of the publication
        suffix (str): Bibliographic references of the publication

    Returns:
        Tuple[str,str]: Tuple of retrieved MR and its link
    """
    first_page, _ = shared.get_page_range_from_suffix(suffix)
    year = shared.get_publication_year_from_suffix(suffix)
    search_pattern = f"||{author}|||{first_page}|{year}||||{title}"
    payload = {
        "api" : "xref",
        "qdata" : search_pattern
    }
    html_text = requests.get("https://mathscinet.ams.org/batchmrlookup",params=payload).text
    mr_number = html_text.split("|")
    if mr_number[9] != "":
        return f"MR{mr_number[9]}",  f"https://mathscinet.ams.org/mathscinet-getitem?mr={mr_number[9]}"
    return "",""