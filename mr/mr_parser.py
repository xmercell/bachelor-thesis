from mr import mr_finder
import shared

def parser(query):
    """This function is used as a parser to retrieve MR identifier with usage of basic attributes 

    Args:
        query (Dict[str]): Dictionary, which contains basic information such as author, title and bibliographic references

    Returns:
        Dict[str]: Dictionary which contains MR identifier with its link
    """
    mr,mr_link = mr_finder.find_mr_and_metadata(query["query.author"],query["query.title"],query["query.bibliographic"])
    
    if mr != "":
        return get_result(mr,mr_link)

    mr_one_author, mr_link_one_author =  mr_finder.find_mr_and_metadata(shared.get_one_author(query["query.author"]),query["query.title"],query["query.bibliographic"])
    
    if mr == "" and mr_one_author != "":
        return get_result(mr_one_author,mr_link_one_author)
    return get_result(mr,mr_link)
    
def get_result(mr,mr_link):
    """This function returns dictionary of MR and its link 

    Args:
        mr (str): MR identifier
        mr_link (str): Link of MR identifier

    Returns:
        Dict[str]: Dictionary which contains MR identifer and its link
    """
    return {
        "mr" : mr,
        "mr_link" : mr_link
    }