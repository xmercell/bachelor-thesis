import requests
from bs4 import BeautifulSoup
from ratelimit import limits, sleep_and_retry
import requests_cache
import shared
from doi import doi_parser


@sleep_and_retry
@limits(calls=50,
        period=1)  # Allows 50 requets within 1 second; sleep till the request period has ellapsed and then retry
def find_mr_and_metadata(author, title, suffix):
    """This function makes an GET request to MathSciNet and retrieves MR with its link

    Args:
        author (str): Author of the publication
        title (str): Title of the publication
        suffix (str): Bibliographic references of the publication

    Returns:
        Tuple[str,str]: Tuple of retrieved MR and its link
    """

    first_page, last_page = shared.get_page_range_from_suffix(suffix)
    year = shared.get_publication_year_from_suffix(suffix)
    year_first, year_second = "", ""
    if year != "":
        year = int(year)
        year_first = year - 1
        year_second = year + 1 
    new_author = shared.get_one_author(author)
    range_pages = ""
    if first_page != "" and last_page != "":
        range_pages = f"{first_page}-{last_page}"

    doi = doi_parser.parser({"query.author":author,"query.title" : title, "query.bibliographic" : suffix})["doi"]

    payload = {
        "pg4": "AUCN",
        "s4": f"{new_author}",
        "co4": "AND",
        "pg5": "TI",
        "s5": f"{title}",
        "co5": "AND",
        "pg6": "ALLF",
        "s6": f"{range_pages}",
        "co6": "OR",
        "pg7": "DOI",
        "s7": f"{doi}",
        "co7": "AND",
        "yrop": "eq",
        "arg3": "",
        "dr": "pubyear",
        "yearRangeFirst": f"{year_first}",
        "yearRangeSecond": f"{year_second}",
        "pg8": "ET",
        "s8": "All",
        "review_format": "html",
        "sort": "citations",
        "Submit": "Search"
    }


    html_text = requests.get("https://mathscinet.ams.org/mathscinet/search/publications.html", params=payload).text
    soup = BeautifulSoup(html_text, features="lxml")

    mr_books = soup.select_one("#content > form > div.row.has-side-left > div.row-body > div > div > div.headline.first > div.headlineText > a.mrnum > strong")
    mr_book = soup.select_one("#content > div.doc > div.headline > strong:nth-child(2)")

    if mr_books is not None:
        mr = mr_books.text
        mr_link = f"https://mathscinet.ams.org/mathscinet-getitem?mr={mr[2:]}"
        return mr, mr_link

    if mr_book is not None:
        mr = mr_book.text
        mr_link = f"https://mathscinet.ams.org/mathscinet-getitem?mr={mr[2:]}"
        return mr, mr_link
    return "", ""


def check_access_to_advanced_search():
    """This function checks if a user has permission to search on website MathSciNet

    Returns:
        bool: True, if user has permission to search on website MathSciNet, False otherwise
    """
    payload = {
        "pg4": "AUCN",
        "s4": "Fučík S.",
        "co4": "AND",
        "pg5": "TI",
        "s5": "Solvability of nonlinear equations and boundary value problems",
        "co5": "AND",
        "pg6": "ALLF",
        "s6": "",
        "co6": "OR",
        "pg7": "DOI",
        "s7": "",
        "co7": "AND",
        "yrop": "eq",
        "arg3": "",
        "dr": "pubyear",
        "yearRangeFirst": "1979",
        "yearRangeSecond": "1981",
        "pg8": "ET",
        "s8": "All",
        "review_format": "html",
        "sort": "citations",
        "Submit": "Search"
    }

    with requests_cache.disabled():
        html_text = requests.get("https://mathscinet.ams.org/mathscinet/search/publications.html", params=payload).text
    soup = BeautifulSoup(html_text, features="lxml")

    access = soup.find("title").text
    if access == "MathSciNet Access Error":
        return False
    return True
