from mr import mr_advanced_finder

def parser(query):
    """This function is used as a parser to retrieve MR identifier with usage of extended attributes 

    Args:
        query (Dict[str]): Dictionary, which contains basic information such as author, title and bibliographic references

    Returns:
        Dict[str]: Dictionary which contains MR identifier with its link
    """
    mr,mr_link = mr_advanced_finder.find_mr_and_metadata(query["query.author"],query["query.title"],query["query.bibliographic"])
    return {
        "mr" : mr,
        "mr_link" : mr_link
    }
