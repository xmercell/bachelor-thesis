import requests
from bs4 import BeautifulSoup
from ratelimit import limits, sleep_and_retry
import shared

@sleep_and_retry
@limits(calls=50,
        period=1)  # Allows 50 requets within 1 second; sleep till the request period has ellapsed and then retry
def find_zbl_and_metadata(author, title, suffix):
    """This function makes an GET request to zbMATH and retrieves ZBL identifier with its link

    Args:
        author (str): Author of the publication
        title (str): Title of the publication
        suffix (str): Bibliographic references of the publication

    Returns:
        Tuple[str,str]: Tuple of retrieved ZBL and its link
    """

    year = shared.get_publication_year_from_suffix(suffix)

    first_page, last_page = shared.get_page_range_from_suffix(suffix)
    page_range = ""
    if first_page is not None and last_page is not None:
        page_range = f"{first_page}-{last_page}"

    author = raise_author_accuracy(author)

    payload = {
        "q": f"au:{author} & ti:{title} & (py:{year} | any:{page_range})",
        "r": "relevance"
    }

    html_text = requests.get(f"https://zbmath.org",params=payload).text
    soup = BeautifulSoup(html_text, features="lxml")

    books = soup.find("div", class_="list")
    book = soup.find("div", class_="item")

    if books is not None:
        zbl = books.find("a", class_="label").text
        zbl_link = books.find("a", class_="label").get("href")
        return zbl,f"https://zbmath.org{zbl_link}"

    if book is not None:
        zbl = book.find("h2", class_="title").find("a", class_="label").text
        zbl_link = book.find("h2", class_="title").find("a", class_="label").get("href")
        return zbl, f"https://zbmath.org{zbl_link}"

    return "", ""


def raise_author_accuracy(author):
    """This function modifies author for better accuracy and better results on website zbMATH
       for instance it modifies author Martinson L.O. to Martinson L. O.

    Args:
        author (str): Author of the publication

    Returns:
        str: Modified author for better results on website zbMATH
    """
    if author.count(".") <= 1:
        return author

    new_author = ""
    for index, character in enumerate(author):
        new_author += character
        if author[index-1].isupper() and character == "." and (index+1) < len(author) and author[index+1].isupper():
            new_author += " "
    return new_author
