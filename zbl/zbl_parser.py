from zbl import zbl_finder

def parser(query):
    """This function is used as a parser to retrieve ZBL identifier with its link

    Args:
        query (Dict[str]): Dictionary, which contains basic information such as author, title and bibliographic references

    Returns:
        Dict[str]: Dictionary which contains ZBL identifier with its link
    """
    zbl, zbl_link = zbl_finder.find_zbl_and_metadata(query["query.author"],query["query.title"],query["query.bibliographic"])
    return {
        "zbl" : zbl,
        "zbl_link": zbl_link
    }