import argparse
import json
import requests

#URL of the server
server_url = 'http://localhost:5000/'

def add_to_group(input_file_group, endpoint):
    """This function creates mutually exlusive group with given endpoint 

    Args:
        input_file_group (Any): Mutually exlusive group
        endpoint (str): endpoint, where the request is going to be sent - /doi , /zbl, /mr, /all
    """
    input_file_group.add_argument(
        '-x_'+endpoint, '--xml_input_'+endpoint, help='Input references.xml file', type=str)
    input_file_group.add_argument(
        '-j_'+endpoint, '--json_input_'+endpoint,  help='Input JSON file with references to resolve', type=str)
    input_file_group.add_argument(
        '-t_'+endpoint, '--txt_input_'+endpoint,  help='Input txt file with references to resolve', type=str)

def parse_cmd_arguments():
    """This function proccesses the arguments given in command line

    Returns:
        Any: Arguments which were parsed
    """
    parser = argparse.ArgumentParser(
        description='Get DOIs/ZBLs/MRs from bibliographic references')
    input_file_group = parser.add_mutually_exclusive_group(required=True)

    endpoints = ['doi','mr','zbl','all']
    for endpoint in endpoints:
        add_to_group(input_file_group,endpoint)

    parser.add_argument(
        '-o', '--output',  help='Output JSON file with looked up DOIs/ZBLs/MRs', type=str)
    return parser.parse_args()

def parse_argument(input,endpoint,file_format):
    """Parse argument, opens input file and creates endpoint and Content-Type

    Args:
        input (Any): File 
        endpoint (str): endpoint, which is going to be used while URL routing
        file_format (str): format of the file etc. xml, json, txt

    Returns:
        Tuple[str, str, bytes]: Tuple consisting of Content-Type, Url and data, which are going to be sent as request
    """
    if input is not None:
        with open(input, encoding="utf-8") as file:
            data_to_send = file.read()

        if file_format == "txt":
            headers = {"Content-Type": "text/plain"}
        else:
            headers = {"Content-Type": f"application/{file_format}"}

        url = f"{server_url}{endpoint}"
        headers.update({"Accept": "application/json"})
        return headers, url, data_to_send
    return None,None,None

def main() -> None:
    """Read input file and writes into output file extended by specified identifier, both specified in command line arguments 
    """
    args = parse_cmd_arguments()
    file_to_write = args.output
    data_to_send = ""
    headers = {}
    url = ""

    inputs = [
        (args.xml_input_doi,'doi','xml'),
        (args.json_input_doi,'doi','json'),
        (args.txt_input_doi,'doi','txt'),

        (args.xml_input_zbl,'zbl','xml'),
        (args.json_input_zbl,'zbl','json'),
        (args.txt_input_zbl,'zbl','txt'),

        (args.xml_input_mr,'mr','xml'),
        (args.json_input_mr,'mr','json'),
        (args.txt_input_mr,'mr','txt'),

        (args.xml_input_all,'all','xml'),
        (args.json_input_all,'all','json'),
        (args.txt_input_all,'all','txt'),

    ]

    for args_input, endpoint, file_format in inputs:
        headers, url,data_to_send = parse_argument(args_input, endpoint,file_format)
        if headers is not None and url is not None and data_to_send is not None :
            response = requests.post(
                url, data=data_to_send.encode("utf-8"), headers=headers)
            output_json = json.loads(response.text)
            if file_to_write is None:
                print(response.text.encode('utf8'))
                return
            with open(file_to_write, 'w', encoding="utf-8") as output_file:
                json.dump(output_json, output_file)


if __name__ == '__main__':
    main()
