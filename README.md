# An Automated System for Lookup Persistent Identifiers for Mathematical Articles and Their Reviews

Make sure you have available port 5000 and it's not allocated by another application.

Running application using docker where build of container is done locally
------------

In a root directory of this project use following command to build a docker container:

```
$ docker build -t automation-system .
```

Use following command to run docker container:

```
$ docker-compose up
```

Running application using docker and download image from Docker Hub
------------

Use following command to run docker container, in a directory where you cloned this repository 
```
$ docker run --name automated-lookup-system -p 5000:5000 -v {Here is absolute path to bachelor-thesis folder}:/bachelor-thesis petermercell/persistent-identifiers-lookup-system
```         


Retrieving DOI/MR/ZBL identifiers using curl
------------

You can see all necessary information documented using SWAGGER UI with your browser, when you run application using docker and then in your browser go to and see each documented endpoint
```
http://127.0.0.1:5000
```

Retrieving identifiers using python
------------

Note that now python client is now in docker container. So all paths to files must be in linux way

Retrieving DOI using python
------------

Use a following command to retrieve DOI from xml file :

```
$ docker exec automated-lookup-system python client.py -x_doi {path to xml file in docker container, for instance sample_data/more_books/sample.xml} -o {json output file name, for instance output.json}
```

Use a following command to retrieve DOI from txt file :

```
$ docker exec automated-lookup-system python client.py -t_doi {path to txt file in docker container, for instance sample_data/more_books/sample.txt} -o {json output file name, for instance output.json}
```

Use a following command to retrieve DOI from json file :

```
$ docker exec automated-lookup-system python client.py -j_doi {path to json file in docker container, for instance sample_data/more_books/sample.json} -o {json output file name, for instance output.json}
```

Retrieving ZBL using python
------------

Use a following command to retrieve ZBL from xml file :

```
$ docker exec automated-lookup-system python client.py -x_zbl {path to xml file in docker container, for instance sample_data/more_books/sample.xml} -o {json output file name, for instance output.json}
```

Use a following command to retrieve ZBL from txt file :

```
$ docker exec automated-lookup-system python client.py -t_zbl {path to txt file in docker container, for instance sample_data/more_books/sample.txt} -o {json output file name, for instance output.json}
```

Use a following command to retrieve ZBL from json file :

```
$ docker exec automated-lookup-system python client.py -j_zbl {path to json file in docker container, for instance sample_data/more_books/sample.json} -o {json output file name, for instance output.json}
```

Retrieving MR using python
------------

Use a following command to retrieve MR from xml file :

```
$ docker exec automated-lookup-system python client.py -x_mr {path to xml file in docker container, for instance sample_data/more_books/sample.xml} -o {json output file name, for instance output.json}
```

Use a following command to retrieve MR from txt file :

```
$ docker exec automated-lookup-system python client.py -t_mr {path to txt file in docker container,for instance sample_data/more_books/sample.txt} -o {json output file name, for instance output.json}
```

Use a following command to retrieve MR from json file :

```
$ docker exec automated-lookup-system python client.py -j_mr {path to json file in docker container, for instance sample_data/more_books/sample.json} -o {json output file name, for instance output.json}
```


Retrieving all identifiers - DOI, ZBL and MR using python
------------

Use a following command to retrieve all identifiers from xml file :

```
$ docker exec automated-lookup-system python client.py -x_all {path to xml file in docker container, for instance sample_data/more_books/sample.xml} -o {json output file name, for instance output.json}
```

Use a following command to retrieve all identifiers from txt file :

```
$ docker exec automated-lookup-system python client.py -t_all {path to txt file in docker container, for instance sample_data/more_books/sample.txt} -o {json output file name, for instance output.json}
```

Use a following command to retrieve all identifiers from json file :

```
$ docker exec automated-lookup-system python client.py -j_all {path to json file in docker container, for instance sample_data/more_books/sample.json} -o {json output file name, for instance output.json}
```

Note
------------
If you want to use Python file client.py outside of this container, you have to install requirements, which are used by client.
```
$ pip install argparse
$ pip install json
$ pip install requests
```
Then you can use client.py without docker exec automated-lookup-system command, for instance :

```
$ python client.py -j_all {path to json file} -o {json output file name, for instance output.json}
```
