from doi import doi_finder


def parser(query):
    """This function is used as a parser to retrieve DOI identifier

    Args:
        query (Dict[str]): Dictionary, which contains basic information such as author, title and bibliographic references

    Returns:
        Dict[str]: Dictionary which contains DOI identifier with its link and score
    """
    attribute_meta_data = doi_finder.find_doi_using_api(query)
    text_meta_data = doi_finder.find_doi_using_api(doi_finder.create_query_using_text(query))
    correct_doi, correct_doi_score = doi_finder.decide_correctness_of_doi(attribute_meta_data, text_meta_data)
    doi_link = f"https://doi.org/{correct_doi}" if correct_doi != "" else ""
    return {
        "doi": correct_doi,
        "doi score": correct_doi_score,
        "doi_link": doi_link
    }
