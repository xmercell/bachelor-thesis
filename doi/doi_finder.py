import copy
from crossref_commons.iteration import iterate_publications_as_json
from ratelimit import limits, sleep_and_retry

# Thresholds for CrossRef reference lookup score
# if results by doi_text and doi_attribute are not the same and score is lower than this, DOI is not returned as the result is considered unreliable
score_threshold = 100
# if results by doi_text and doi_attribute are the same and score is lower than this, DOI is not returned as the result is considered unreliable
score_treshold_text_equal_to_attributes = 64.6


@sleep_and_retry
@limits(calls=50,
        period=1)  # Allows 50 requests within 1 second; sleep till the request period has ellapsed and then retry
def find_doi_using_api(queries):
    """This function calls API of Crossref and retrieves DOI object with its score

    Args:
        queries (Dict[str]): Dictionary, which contains basic information such as author, title and bibliographic references

    Returns:
        Tuple[str,str]: Tuple of retrieved DOI and DOI score
    """
    for p in iterate_publications_as_json(max_results=1, queries=copy.deepcopy(queries)):
        return p["DOI"], p["score"]


def decide_correctness_of_doi(attribute_meta_data, text_meta_data):
    """This function checks if DOI retrieved from API Crossref call is correct or not 

    Args:
        attribute_meta_data (Tuple[str,str]): Tuple of DOI and DOI score, which was retrieved by request constructed by attributes (author, name, bibliographic references)
        text_meta_data (Tuple[str,str]): Tuple of DOI and DOI score, which was retrieved by request constructed by concatenation of attributes into bibliographic references

    Returns:
        Tuple[str,str]: DOI and DOI score if the value is correct, empty string for DOI and empty string for DOI score otherwise
    """
    if attribute_meta_data is not None and text_meta_data is not None:
        doi_using_attributes, doi_using_attributes_score = attribute_meta_data
        doi_using_text, doi_using_text_score = text_meta_data
        maximal_value = max(float(doi_using_text_score),float(doi_using_attributes_score))
        if doi_using_attributes == doi_using_text and maximal_value > score_treshold_text_equal_to_attributes:
            return doi_using_text, str(maximal_value)

    if attribute_meta_data is not None and text_meta_data is not None:
        doi_using_attributes, doi_using_attributes_score = attribute_meta_data
        doi_using_text, doi_using_text_score = text_meta_data
        if float(doi_using_attributes_score) > float(doi_using_text_score) and float(doi_using_attributes_score) > score_threshold:
            return doi_using_attributes , doi_using_attributes_score
        elif float(doi_using_text_score) > float(doi_using_attributes_score) and float(doi_using_text_score) > score_threshold:
            return doi_using_text, doi_using_text_score

    if attribute_meta_data is not None and text_meta_data is None:
        doi_using_attributes, doi_using_attributes_score = attribute_meta_data
        if float(doi_using_attributes_score) > score_threshold:
            return doi_using_attributes, doi_using_attributes_score

    if text_meta_data is not None and attribute_meta_data is None:
        doi_using_text, doi_using_text_score = text_meta_data
        if float(doi_using_text_score) > score_threshold:
            return doi_using_text, doi_using_text_score

    return "", ""


def create_query_using_text(query):
    """This function concatenate attributes of the query - author, title and bibliographic references into one attribute - bibliographic references

    Args:
        query (Dict[str]): Dictionary, which contains basic information such as author, title and bibliographic references

    Returns:
        Dict[str]: Dictionary, which contains joined attributes into one attribute of bibliographic references
    """
    query_using_text = {
        "query.bibliographic": ', '.join([query["query.author"], query["query.title"], query["query.bibliographic"]])}
    return query_using_text