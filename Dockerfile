FROM python:3.10-alpine

RUN apk update && apk add gcc libc-dev make git libffi-dev openssl-dev python3-dev libxml2-dev libxslt-dev

RUN pip install https://github.com/rochacbruno/flasgger/tarball/master

WORKDIR /bachelor-thesis

COPY requirements.txt .

RUN pip install -r requirements.txt

COPY . .

CMD ["python","web_server.py"]